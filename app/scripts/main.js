/*eslint-disable */

// Page-nav--------------------------------------------
var ww = document.body.clientWidth;

$(document).ready(function() {
  $(".page-nav li a").each(function() {
    if ($(this).next().length > 0) {
      $(this).addClass("parent");
    }
  });

  $(".toggleMenu").click(function(e) {
    e.preventDefault();
    $(this).toggleClass("active");
    $(this).toggleClass("open");
    $(this).toggleClass("toggleMenu--bg-color");
    $(".page-nav").toggle();
  });
  adjustMenu();
});

$(window).bind('resize orientationchange', function() {
  ww = document.body.clientWidth;
  adjustMenu();
});

var adjustMenu = function() {
  if (ww < 768) {
    $(".toggleMenu").css("display", "inline-block");
    if (!$(".toggleMenu").hasClass("active")) {
      $(".page-nav").hide();
    } else {
      $(".page-nav").show();
    }
    $(".page-nav li").unbind('mouseenter mouseleave');
    $(".page-nav li a.parent").unbind('click').bind('click', function(e) {
      // must be attached to anchor element to prevent bubbling
      e.preventDefault();
      $(this).parent("li").toggleClass("hover");
    });
  }
  else if (ww >= 768) {
    $(".page-nav").show();
    $(".page-nav li").removeClass("hover");
    $(".page-nav li a").unbind('click');
    $(".page-nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
      // must be attached to li so that mouseleave is not triggered when hover over submenu
      $(this).toggleClass('hover');
    });
  }
};


// Owl-Carousel---------------------------------------
jQuery(document).ready(function($) {

  var owl = $('.owl-carousel');
  var compress = $('.owl-carousel__compress');
  var owlNav = $('.owl-nav');

  compress.on('click', function () {
    owlNav.slideUp();
    return false;
  });

  owl.owlCarousel({
      onChanged: count,
      onRefreshed: count,
      autoplay: true,
      loop:true,
      margin:10,
      nav:true,
      navContainer: '.owl-nav',
      responsive:{
          0:{
              items:1
          }
      }
  });

  function count(event) {
      var items     = event.item.count;     // Number of items
      var item      = event.item.index;     // Position of the current item
      // Provided by the navigation plugin
      var pages     = event.page.count;     // Number of pages
      var page      = event.page.index;     // Position of the current page

      if($('.owl-nav__item').html() === '-1' || $('.owl-nav__item').html() === '0') {
        $('.owl-nav__item').html('1');
      } else {
        $('.owl-nav__item').html(page+1);
      }

      if ($('.owl-nav__count').html() === '0') {
        $('.owl-nav__count').html(items);
      } else {
        $('.owl-nav__count').html(items);
      }
  }

});
